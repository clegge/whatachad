package net.ideablender.whatAChad

import net.ideablender.whatAChad.tests.WLogTests

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    WLogTests::class
)
class TestSuite {
}