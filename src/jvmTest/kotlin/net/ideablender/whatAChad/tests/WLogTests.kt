package net.ideablender.whatAChad.tests


import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import net.ideablender.whatAChad.TestSession
import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast
import net.ideablender.whatAChad.helpers.WarcraftLogHelper
import net.ideablender.whatAChad.pojos.WLRaidExecution
import net.ideablender.whatAChad.pojos.WLReport
import net.ideablender.whatAChad.utils.toNative
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import java.util.*
import java.util.concurrent.TimeUnit


data class CastEntry(val guid: Int, val name: String)

fun duration(start: Date, end: Date): String {
    val millis = end.time - start.time
    val foo = String.format(
        "%d min, %d sec",
        TimeUnit.MILLISECONDS.toMinutes(millis),
        TimeUnit.MILLISECONDS.toSeconds(millis) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
    );
    return foo
}

fun WLRaidExecution.getDuration(): String {
    return duration(this.timeStart.toNative(), this.timeEnd.toNative())
}

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class WLogTests {
    private val wlReportKey = "mdc7rYMzTw8aLPC4"

    @Test
    fun `Test 1 get reports`() {
        val reports: Array<WLReport> = WarcraftLogHelper.fetchReports()
        assertTrue("reports has data", reports.size > 0)
    }

    @Test
    fun `Test 2 get zones`() {
        WarcraftLogHelper.fetchZones()
        val zones = WarcraftLogHelper.getZones()
        val mc = zones.find { it.name == "Molten Core" }
        assertTrue("Molten Core exists", mc != null)
        assertTrue("reports has data", zones.size > 0)
    }

    @Test
    fun `Test 3 get RaidExecution`() {
        TestSession.raidExecution = WarcraftLogHelper.fetchRaidExecution(wlReportKey)
        assertNotNull("fetchRaidExecution executed successfully", TestSession.raidExecution)
    }

    @Test
    fun `Test 4 warrior report card`() {
        assertNotNull("We have the raid execution built in test 3", TestSession.raidExecution)
        val raidCtx = WarcraftLogHelper.buildCTXRaid(wlReportKey)
        val tomReport = raidCtx.reports.find { it.warrior.name == "Tomhighway" }
        assertNotNull("Toms report exists", tomReport)
        assertTrue("Tom casted 245 bloodthirst", tomReport!!.getBloodThirst()?.count == 245)
        assertTrue("Tom casted 41 sundered", tomReport!!.getCastTotalFor(NotableCast.SUNDER_ARMOR)?.count == 41)
        assertTrue("Tom used diamond flask 6 times", tomReport.getCastTotalFor(NotableCast.DIAMOND_FLASK)?.count == 6)
        assertTrue("Tom used mongoose 2 times", tomReport.getBuffTotalFor(NotableBuff.ELIXIR_MONGOOSE)?.count == 2)
    }


    @Test
    fun `Test 5 import raid`() {
        WarcraftLogHelper.createRaid(WarcraftLogHelper.getReports().first())
    }
}