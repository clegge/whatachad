import io.ktor.client.features.websocket.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.w3c.dom.WebSocket
import react.dom.render
import kotlin.browser.document

//https://github.com/shyiko/skedule

val scope = MainScope()
fun main() {
/*    scope.launch {
        client.ws(
            method = HttpMethod.Get,
            host = "127.0.0.1",
            port = 9090, path = "/ws"
        ) { // this: DefaultClientWebSocketSession

            // Send text frame.
            send("Hello, Text frame")

            // Send text frame.
            // send(Frame.Text("Hello World"))

            // Receive frame.
            val frame = incoming.receive()
            when (frame) {
                is Frame.Text -> console.log(frame.readText())
                is Frame.Binary -> console.log(frame.readBytes())
            }
        }
    }

    val ws = WebSocket("ws://localhost:9090/ws")
    ws.onopen = {
        ws.send("Message to send")
    }
    ws.onmessage = {
        console.log("Websocker receiver", it.data)
        scope.launch {

        }
    }*/

    render(document.getElementById("root")) {
        child(Index::class){}
    }
}