import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.http.*
import net.ideablender.whatAChad.dtos.*
import net.ideablender.whatAChad.req.LoginReq


import net.ideablender.whatAChad.store.Store
import org.w3c.fetch.Response.Companion.redirect
import react.router.dom.redirect
import kotlin.browser.window

val endpoint = window.location.origin // only needed until https://github.com/ktorio/ktor/issues/1695 is resolved

val jsonClient = HttpClient {
    install(JsonFeature) { serializer = KotlinxSerializer() }
}

suspend fun getFooBar():String{
    return jsonClient.get(endpoint + "/foobar")
}

suspend fun loginUser(req:LoginReq){
    val dto = jsonClient.post<LoginDTO>(endpoint + LoginReq.getPath()) {
        contentType(ContentType.Application.Json)
        body = req
    }
    if(dto.success){
        Store.setToken(dto.token ?: throw Exception("loginUser response was a success but no token present"))
    }
}




suspend fun getWLReports(){
    val payload =  jsonClient.get<List<WLReportDTO>>(endpoint + WLReportDTO.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setWlReports(payload)
}
//RaidMemberDTO.getPath()
suspend fun fetchRaidMembers(){
    val payload =  jsonClient.get<List<RaidMemberDTO>>(endpoint + RaidMemberDTO.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setRaidMembers(payload)
}
suspend fun fetchRaids(){
    val payload =  jsonClient.get<List<RaidDto>>(endpoint + RaidDto.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setRaids(payload)
}

suspend fun fetchZones(){
    val payload =  jsonClient.get<List<ZoneDTO>>(endpoint + ZoneDTO.getPath())
    Store.setZones(payload)
}

suspend fun fetchReportCards(){
    val payload =  jsonClient.get<List<WarriorReportCardDTO>>(endpoint + WarriorReportCardDTO.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setReportCards(payload)
}


suspend fun fetchBuffTotals(){
    val payload =  jsonClient.get<List<BuffTotalDTO>>(endpoint + BuffTotalDTO.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setBuffTotals(payload)
}

suspend fun fetchCastTotals(){
    val payload =  jsonClient.get<List<CastTotalDTO>>(endpoint + CastTotalDTO.getPath()){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    Store.setCastTotals(payload)
}

suspend fun getProtected():String{
    val payload =  jsonClient.get<String>(endpoint + "/protected"){
        header("Authorization", "Bearer ${Store.getToken()}")
    }
    return payload
}

suspend fun getProtectedFail():String{
    val payload =  jsonClient.get<String>(endpoint + "/protected")
    return payload
}







