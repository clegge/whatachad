package net.ideablender.whatAChad.pages

import fetchBuffTotals
import fetchCastTotals
import fetchRaidMembers
import fetchRaids
import fetchReportCards
import fetchZones
import getWLReports
import kotlinx.coroutines.launch
import net.ideablender.whatAChad.store.Store
import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.*
import react.dom.h2
import scope

class PAdmin : RComponent<RProps, RState>() {
    override fun componentDidMount() {
        Store.registerCallback("PAdmin", ::handleChange)
        if (Store.getWLReports().count() == 0) {
            scope.launch {
                getWLReports()
                fetchRaidMembers()
                fetchZones()
                fetchRaids()
                fetchReportCards()
                fetchBuffTotals()
                fetchCastTotals()
            }
        }
    }

    private fun handleChange() {
        forceUpdate()
    }

    override fun RBuilder.render() {
        section("wrapper PAdmin") {
            h2 {
                +"Admin"
            }
            if (Store.getWLReports().count() > 0) {
                h3 {
                    +"Available Raids on Warcraft Logs"
                }
                ul {
                    val list = Store.getWLReports()
                    list.sortBy { it.started.jdTime }
                    list.forEach {
                        li {
                            +"${it.started.formatShort()} ${it.title} (${it.id})"
                        }
                    }
                }
            }
            if (Store.getRaidMembers().count() > 0) {
                h3 {
                    +"Raiders"
                }
                ul {
                    val list = Store.getRaidMembers()
                    list.sortBy { it.name }
                    list.forEach {
                        li {
                            +"${it.name}"
                        }
                    }
                }
            }
            if (Store.getRaids().count() > 0) {
                h3 {
                    +"Raids Processed - ${Store.getRaids().count()} "
                }
            }

            if (Store.getZones().count() > 0) {
                h3 {
                    +"Zones - ${Store.getZones().count()} "
                }
            }
            if (Store.getReportCards().count() > 0) {
                h3 {
                    +"Report Cards - ${Store.getReportCards().count()} "
                }
            }
            if (Store.getBuffTotals().count() > 0) {
                h3 {
                    +"Buffs - ${Store.getBuffTotals().count()} "
                }
            }
            if (Store.getCastTotals().count() > 0) {
                h3 {
                    +"Casts - ${Store.getCastTotals().count()} "
                }
            }
        }
    }
}