package net.ideablender.whatAChad.pages

import kotlinx.html.js.onClickFunction
import net.ideablender.whatAChad.dtos.BuffTotalDTO
import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.dtos.RaidDto
import net.ideablender.whatAChad.enums.NotableCast
import net.ideablender.whatAChad.enums.WarriorSpec
import net.ideablender.whatAChad.store.Store
import net.ideablender.whatAChad.utils.*
import react.*
import react.dom.*
import kotlin.browser.window

interface PRaidState : RState {
    var raidList: List<RaidDto>
    var raidId: Int?
    var reportCardId: Int?
}

class PRaid : RComponent<RProps, PRaidState>() {
    override fun PRaidState.init() {
        raidList = Store.getRaids()
    }

    override fun componentDidMount() {
        setState {
            raidList = Store.getRaids()
        }
    }

    override fun RBuilder.render() {
        section("wrapper KRaids") {
            h2 {
                +"Guild Raids"
            }
            table("table table-striped table-bordered") {
                thead("thead-dark") {
                    tr {
                        th {
                            +"Title"
                        }
                        th {
                            +"Date"
                        }
                        th {
                            +"Zone"
                        }
                        th {
                            +"Warcraft Logs Id"
                        }
                        th {
                            +"Report Count"
                        }
                    }
                }
                tbody {
                    state.raidList.forEach { raidDto ->
                        tr("clickable") {
                            attrs.onClickFunction = {
                                setState {
                                    raidId = raidDto.id
                                    reportCardId = null
                                }
                            }
                            td {
                                +raidDto.title
                            }
                            td {
                                +raidDto.date.formatShort()
                            }
                            td {
                                +Store.getZones().find { it.id == raidDto.wlZoneId }?.name!!
                            }
                            td {
                                +raidDto.uuid
                            }
                            td {
                                +raidDto.reports.count().toString()
                            }
                        }
                    }
                }
            }

            if (state.reportCardId != null) {
                val reporCard = Store.getReportCards().find { it.id == state.reportCardId }
                    ?: throw (Exception("Store.getReportCards() does not contain ${state.reportCardId}"))

                showReportCard(reporCard){
                    setState {
                        reportCardId = null
                    }
                }

            }
            if (state.raidId != null) {
                val raidDto = Store.getRaids().find { it.id == state.raidId }
                    ?: throw (Exception("Store.getRaids() does not contain ${state.raidId}"))
                val reports = Store.getReportCards()
                    .filter { raidDto.reports.map { reportsRaid -> reportsRaid.id }.contains(it.id) }
                div("card") {
                    h5("card-header") {
                        +"${raidDto.title} Warrior Reports"
                    }
                    div("card-body") {
                        if (reports.map { it.spec }.contains(WarriorSpec.FURY_PROT.name)) {
                            h4 {
                                +"Fury/Prot overview"
                            }
                            table("table table-striped table-bordered table-sm") {
                                thead("thead-dark") {
                                    tr {
                                        th {
                                            +"Raider"
                                        }
                                        th {
                                            +"Sunder Count"
                                        }
                                        th {
                                            +"Hs Count"
                                        }
                                        th {
                                            +"BT Count"
                                        }
                                        th {
                                            +"Greater Armor Count"
                                        }
                                        th {
                                            +"Greater Stone Shield"
                                        }
                                        th {
                                            +"Mongoose Count"
                                        }
                                    }
                                }
                                tbody {
                                    reports.forEach { reportCard ->
                                        if (reportCard.spec == WarriorSpec.FURY_PROT.name) {
                                            console.log(reportCard)
                                            val sunderId =
                                                reportCard.casts.find { cast -> cast.detail == NotableCast.SUNDER_ARMOR.description }?.id
                                            val castSunder = Store.getCastTotals().find { it.id == sunderId }
                                            console.log(castSunder)
                                            tr("clickable") {
                                                attrs.onClickFunction = {
                                                    setState {
                                                        reportCardId = reportCard.id
                                                    }
                                                }
                                                td {
                                                    +reportCard.raidMember.detail
                                                }
                                                td {
                                                    +reportCard.getSunderCount().toString()
                                                }
                                                td {
                                                    +reportCard.getHeroicStrikeCount().toString()
                                                }
                                                td {
                                                    +reportCard.getBloodThirstCount().toString()
                                                }
                                                td {
                                                    +reportCard.getGreaterArmor().toString()
                                                }
                                                td {
                                                    +reportCard.getStoneShield().toString()
                                                }
                                                td {
                                                    +reportCard.getMongooseCount().toString()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (reports.map { it.spec }.contains(WarriorSpec.PROT.name)) {
                            h4 {
                                +"Prot overview"
                            }
                            table("table table-striped table-bordered table-sm") {
                                thead("thead-dark") {
                                    tr {
                                        th {
                                            +"Raider"
                                        }
                                        th {
                                            +"Sunder Count"
                                        }
                                        th {
                                            +"Hs Count"
                                        }
                                        th {
                                            +"Shield Slam"
                                        }
                                        th {
                                            +"Greater Armor Count"
                                        }
                                        th {
                                            +"Greater Stone Shield"
                                        }
                                        th {
                                            +"Mongoose Count"
                                        }
                                    }
                                }
                                tbody {
                                    reports.forEach { reportCard ->
                                        if (reportCard.spec == WarriorSpec.PROT.name) {
                                            console.log(reportCard)
                                            val sunderId =
                                                reportCard.casts.find { cast -> cast.detail == NotableCast.SUNDER_ARMOR.description }?.id
                                            val castSunder = Store.getCastTotals().find { it.id == sunderId }
                                            console.log(castSunder)
                                            tr("clickable") {
                                                attrs.onClickFunction = {
                                                    setState {
                                                        reportCardId = reportCard.id
                                                    }
                                                }
                                                td {
                                                    +reportCard.raidMember.detail
                                                }
                                                td {
                                                    +reportCard.getSunderCount().toString()
                                                }
                                                td {
                                                    +reportCard.getHeroicStrikeCount().toString()
                                                }
                                                td {
                                                    +reportCard.getShieldSlamCount().toString()
                                                }
                                                td {
                                                    +reportCard.getGreaterArmor().toString()
                                                }
                                                td {
                                                    +reportCard.getStoneShield().toString()
                                                }
                                                td {
                                                    +reportCard.getMongooseCount().toString()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (reports.map { it.spec }.contains(WarriorSpec.FURY.name)) {
                            h4 {
                                +"Fury overview"
                            }
                            table("table table-striped table-bordered table-sm") {
                                thead("thead-dark") {
                                    tr {
                                        th {
                                            +"Raider"
                                        }
                                        th {
                                            +"Sunder Count"
                                        }
                                        th {
                                            +"Hs Count"
                                        }
                                        th {
                                            +"Blood Thirst Count"
                                        }
                                        th {
                                            +"Whirlwind Count"
                                        }
                                        th {
                                            +"Giants Count"
                                        }
                                        th {
                                            +"Firewater Count"
                                        }
                                        th {
                                            +"Mongoose Count"
                                        }
                                    }
                                }
                                tbody {
                                    reports.forEach { reportCard ->
                                        if (reportCard.spec == WarriorSpec.FURY.name) {
                                            tr("clickable") {
                                                attrs.onClickFunction = {
                                                    setState {
                                                        reportCardId = reportCard.id
                                                    }
                                                }
                                                td {
                                                    +reportCard.raidMember.detail
                                                }
                                                td {
                                                    +reportCard.getSunderCount().toString()
                                                }
                                                td {
                                                    +reportCard.getHeroicStrikeCount().toString()
                                                }
                                                td {
                                                    +reportCard.getBloodThirstCount().toString()
                                                }
                                                td {
                                                    +reportCard.getWhirlWindCount().toString()
                                                }
                                                td {
                                                    +reportCard.getGiantsCount().toString()
                                                }
                                                td {
                                                    +reportCard.getFireWaterCount().toString()
                                                }
                                                td {
                                                    +reportCard.getMongooseCount().toString()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        div("card-footer") {
                            span {
                                attrs.onClickFunction = {
                                    setState {
                                        raidId = null
                                    }
                                }
                                +"close me"
                            }
                        }
                    }
                }
            }
        }
    }
}