package net.ideablender.whatAChad.pages

import getProtected
import getProtectedFail
import kotlinx.coroutines.launch
import kotlinx.html.ButtonType
import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onClickFunction
import kotlinx.html.style
import loginUser
import net.ideablender.whatAChad.pojos.FormData
import net.ideablender.whatAChad.req.LoginReq
import net.ideablender.whatAChad.store.Store
import org.w3c.dom.HTMLInputElement
import react.*
import react.dom.*
import react.router.dom.redirect
import scope
import kotlin.browser.document
import kotlin.browser.window


interface PLoginState:RState{
    var req:LoginReq
    var pageHeight:String
}
class PLogin : RComponent<RProps, PLoginState>() {
    override fun PLoginState.init() {
        req = LoginReq("", "")
    }

    override fun componentDidMount() {
        Store.registerCallback("PLogin", ::handleStoreChange)
        setState {
            pageHeight = "${window.innerHeight}px"
        }
    }

    fun handleStoreChange(){
        forceUpdate()
    }

    private fun handleFormChange(fd: FormData){
        when(fd.param){
            "userName" ->{
                setState {
                    req = req.copy(userName = fd.theValue as String)
                }
            }
            "password" ->{
                setState {
                    req = req.copy(password = fd.theValue as String)
                }
            }
        }
    }
    override fun RBuilder.render() {
        if(Store.isUserLoggedIn()){
            redirect(from = "", to = "/about")
        }
        if(state.pageHeight != null){
            section("PLogin") {
                div("cardWrapper"){
                    attrs.jsStyle {
                        height = "${state.pageHeight}"
                    }
                    div("card"){
                        h5("card-header"){
                            +"Login"
                        }
                        div("card-body"){
                            input(type = InputType.text, classes = "form-control") {
                                attrs.placeholder = "Username"
                                attrs.value = state.req.userName ?: ""
                                attrs.onChangeFunction = {
                                    val target = it.target as HTMLInputElement
                                    handleFormChange(FormData(param = "userName", theValue = target.value))
                                }
                            }

                            input(type = InputType.text, classes = "form-control") {
                                attrs.placeholder = "Password"
                                attrs.value = state.req.password ?: ""
                                attrs.onChangeFunction = {
                                    val target = it.target as HTMLInputElement
                                    handleFormChange(FormData(param = "password", theValue = target.value))
                                }
                            }
                            button(classes = "btn btn-primary", type = ButtonType.button) {
                                attrs.onClickFunction = {
                                    scope.launch {
                                        loginUser(state.req)
                                    }
                                }
                                +"Login"
                            }
                        }
                    }
                }
            }
        }
    }
}