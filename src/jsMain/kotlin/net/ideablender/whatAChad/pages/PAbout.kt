package net.ideablender.whatAChad.pages

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.h2
import react.dom.section

class PAbout : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        section("wrapper PAbout") {
            h2 {
                +"Welcome"
            }
        }
    }
}