package net.ideablender.whatAChad.pages

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.h2
import react.dom.section

class PHome : RComponent<RProps, RState>() {
    override fun RBuilder.render() {
        section("wrapper PHome") {
            h2 {
                +"Home"
            }
        }
    }
}