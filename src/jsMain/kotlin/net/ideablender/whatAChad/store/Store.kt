package net.ideablender.whatAChad.store

import net.ideablender.whatAChad.dtos.*
import net.ideablender.whatAChad.exceptions.ClientTokenNotFoundException


object Store :Observed() {
    private var _token:String? = null
    private var _redirect:String? = null
    private var _wlReports:MutableList<WLReportDTO> = mutableListOf()
    private var _raidMembers:MutableList<RaidMemberDTO> = mutableListOf()
    private var _raids:MutableList<RaidDto> = mutableListOf()
    private var _reportCards:MutableList<WarriorReportCardDTO> = mutableListOf()
    private var _buffTotals:MutableList<BuffTotalDTO> = mutableListOf()
    private var _castTotals:MutableList<CastTotalDTO> = mutableListOf()
    private var _zones:MutableList<ZoneDTO> = mutableListOf()

    fun setCastTotals(list:List<CastTotalDTO>){
        _castTotals.clear()
        _castTotals.addAll(list)
        invokeCbs()
    }

    fun setZones(list:List<ZoneDTO>){
        _zones.clear()
        _zones.addAll(list)
        invokeCbs()
    }

    fun getZones() = _zones

    fun setBuffTotals(list:List<BuffTotalDTO>){
        _buffTotals.clear()
        _buffTotals.addAll(list)
        invokeCbs()
    }

    fun setWlReports(list: List<WLReportDTO>){
        _wlReports.clear()
        _wlReports.addAll(list)
        invokeCbs()
    }

    fun setReportCards(list: List<WarriorReportCardDTO>){
        _reportCards.clear()
        _reportCards.addAll(list)
        invokeCbs()
    }

    fun setRaidMembers(list: List<RaidMemberDTO>){
        _raidMembers.clear()
        _raidMembers.addAll(list)
        invokeCbs()
    }

    fun setRaids(list: List<RaidDto>){
        _raids.clear()
        _raids.addAll(list)
        invokeCbs()
    }

    fun getCastTotals() = _castTotals
    fun getBuffTotals() = _buffTotals

    fun getReportCards() = _reportCards

    fun getRaids() = _raids

    fun getRaidMembers() = _raidMembers

    fun getWLReports() = _wlReports

    fun setToken(tk:String){
        _token = tk
        invokeCbs()
    }
    fun getToken():String = _token ?: throw ClientTokenNotFoundException()

    fun isUserLoggedIn() = _token != null
}