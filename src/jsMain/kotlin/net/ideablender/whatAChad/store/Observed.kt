package net.ideablender.whatAChad.store

abstract class Observed {
    val cbMap: MutableMap<String, () -> Unit> = mutableMapOf()

    fun registerCallback(key: String, cb: () -> Unit) {
        cbMap[key] = cb
    }

    fun unRegisterCallback(key: String) {
        cbMap.remove(key)
    }

    fun invokeCbs() {
        cbMap.forEach {
            it.value.invoke()
        }
    }
}