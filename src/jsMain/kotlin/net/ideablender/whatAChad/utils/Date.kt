package net.ideablender.whatAChad.utils

actual typealias Date = kotlin.js.Date

fun Date.compareTo(other:Date):Int{
    if (this.getFullYear() > other.getFullYear()) return 1
    if (this.getFullYear() < other.getFullYear()) return -1
    if (this.getMonth() > other.getMonth()) return 1
    if (this.getMonth() < other.getMonth()) return -1
    if (this.getDate() > other.getDate()) return 1
    if (this.getDate() < other.getDate()) return -1
    if (this.getSeconds() > other.getSeconds()) return 1
    if (this.getSeconds() < other.getSeconds()) return -1
    if (this.getMilliseconds() > other.getMilliseconds()) return 1
    if (this.getMilliseconds() < other.getMilliseconds()) return -1
    return 0
}

fun Date.formatShort():String{
    val montAdjusted = this.getMonth() +1
    var hourAdjusted = getHours()
    if(getHours() > 12){
        hourAdjusted = hourAdjusted - 12
    }
    return "${montAdjusted.toTwoDigitString()}/${getDate().toTwoDigitString()}/${getFullYear()} ${hourAdjusted.toTwoDigitString()}:${getMinutes().toTwoDigitString()} ${getHours().toAmPm()}"
}

fun Date.formatShortJustTime():String{
    val montAdjusted = this.getMonth() +1
    var hourAdjusted = getHours()
    if(getHours() > 12){
        hourAdjusted = hourAdjusted - 12
    }
    return "${hourAdjusted.toTwoDigitString()}:${getMinutes().toTwoDigitString()} ${getHours().toAmPm()}"
}

fun Date.isBefore(date:Date):Boolean{
    val isbef = this.compareTo(date)
    return isbef < 0
}

fun Date.isAfter(date:Date):Boolean{
    val isbef = this.compareTo(date)
    return isbef > 0
}


actual fun DateTime.compareToNative(date: Date): Int {
    val nativeAsDT = date.toDateTime()
    return this.compareTo(nativeAsDT)
}

actual fun DateTime.toNative(): Date {
    return Date(year, month, day, hour, minute, second, milliSecond)
}

actual fun DateTime.compareTo(date: DateTime): Int {
    val thisAsNative = this.toNative()
    val compareDate = date.toNative()
    return thisAsNative.compareTo(compareDate)
}

actual fun DateTime.eqNative(date: Date): Boolean =  this.compareToNative(date) == 0

actual fun DateTime.isBefore(date: DateTime): Boolean = this.compareTo(date) < 0

actual fun DateTime.isBeforeNative(date: Date): Boolean = this.compareToNative(date) < 0

actual fun DateTime.isAfter(date: DateTime): Boolean = this.compareTo(date) > 0

actual fun DateTime.isAfterNative(date: Date): Boolean = this.compareToNative(date) > 0

fun kotlin.js.Date.toDateTime(): DateTime = DateTime(
        year = this.getFullYear(),
        month = this.getMonth(),
        day =this.getDate(),
        hour = this.getHours(),
        minute = this.getMinutes(),
        second = this.getSeconds(),
        milliSecond = this.getMilliseconds()
)