package net.ideablender.whatAChad.utils


import kotlinx.html.js.onClickFunction
import net.ideablender.whatAChad.dtos.BuffTotalDTO
import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.dtos.WarriorReportCardDTO
import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast
import net.ideablender.whatAChad.store.Store
import react.RBuilder
import react.dom.*
import react.setState
import kotlin.browser.window

/* CASTS */

private fun getCastCount(dto:WarriorReportCardDTO, _cast: NotableCast):Int{
    val theId = dto.casts.find { cast -> cast.detail == _cast.description }?.id
    val notableCast = Store.getCastTotals().find { it.id == theId }
    return notableCast?.count ?: 0
}
fun WarriorReportCardDTO.getSunderCount(): Int {
    return getCastCount(this, NotableCast.SUNDER_ARMOR)
}

fun WarriorReportCardDTO.getShieldSlamCount(): Int {
    return getCastCount(this, NotableCast.SHIELD_SLAM)
}

fun WarriorReportCardDTO.getWhirlWindCount(): Int {
    return getCastCount(this, NotableCast.WHIRLWIND)
}




fun WarriorReportCardDTO.getHeroicStrikeCount(): Int {
    val theId = this.casts.find { cast ->
        cast.detail == NotableCast.HEROIC_STRIKE_9.description
                || cast.detail == NotableCast.HEROIC_STRIKE_8.description
    }?.id
    val notableCast = Store.getCastTotals().find { it.id == theId }
    return notableCast?.count ?: 0
}

fun WarriorReportCardDTO.getBloodThirstCount(): Int {
    val theId = this.casts.find { cast ->
        cast.detail == NotableCast.BLOODTHIRST_4.description
                || cast.detail == NotableCast.BLOODTHIRST_1.description
    }?.id
    val notableCast = Store.getCastTotals().find { it.id == theId }
    return notableCast?.count ?: 0
}

/* BUFFS */
private fun getBuffCount(dto:WarriorReportCardDTO, _buff: NotableBuff):Int{
    val theId = dto.buffs.find { buff -> buff.detail == _buff.description }?.id
    val notableCast = Store.getBuffTotals().find { it.id == theId }
    return notableCast?.count ?: 0
}

fun WarriorReportCardDTO.getMongooseCount(): Int {
    return getBuffCount(this, NotableBuff.ELIXIR_MONGOOSE)
}

fun WarriorReportCardDTO.getStoneShield(): Int {
    return getBuffCount(this, NotableBuff.GREATER_STONESHIELD)
}

fun WarriorReportCardDTO.getGreaterArmor(): Int {
    return getBuffCount(this, NotableBuff.GREATER_ARMOR)
}

fun WarriorReportCardDTO.getGiantsCount(): Int {
    return getBuffCount(this, NotableBuff.ELIXIR_GIANTS)
}

fun WarriorReportCardDTO.getFireWaterCount(): Int {
    return getBuffCount(this, NotableBuff.WINTERFALL_FIREWATER)
}

fun RBuilder.showReportCard(reporCard:WarriorReportCardDTO, footerClick:()->Unit){
    val castList: MutableList<CastTotalDTO> = mutableListOf()
    val bufflist: MutableList<BuffTotalDTO> = mutableListOf()
    reporCard.casts.forEach { _cast ->
        val cast = Store.getCastTotals().find { it.id == _cast.id }
            ?: throw (Exception("Store.getCastTotals() does not contain ${_cast.id}"))
        castList.add(cast)
    }

    reporCard.buffs.forEach { _buff ->
        val buff = Store.getBuffTotals().find { it.id == _buff.id }
            ?: throw (Exception("Store.getBuffTotals() does not contain ${_buff.id}"))
        bufflist.add(buff)
    }
    castList.sortByDescending { it.count }
    bufflist.sortByDescending { it.count }
    div("card warriorReportCard") {
        h5("card-header") {
            +"${reporCard.raidMember.detail} Report Card"
        }
        div("card-body") {
            h4 {
                +"Fury/Prot overview"
            }
            h5 {
                +"Casts"
            }
            table("table table-striped table-bordered table-sm") {
                thead("thead-dark") {
                    tr("clickable") {
                        th {
                            +"Guid"
                        }
                        th {
                            +"Description"
                        }
                        th {
                            +"Count"
                        }
                    }
                }
                tbody {
                    castList.forEach {
                        tr {
                            td {
                                span(classes = "tdDrillDown") {
                                    attrs.onClickFunction = { evt ->
                                        window.open("https://classic.wowhead.com/spell=${it.guid}")
                                    }
                                    //11597/
                                    +it.guid
                                }
                            }
                            td {
                                +it.description
                            }
                            td {
                                +it.count.toString()
                            }
                        }
                    }
                }
            }
            h5 {
                +"Buffs"
            }
            table("table table-striped table-bordered table-sm") {
                thead("thead-dark") {
                    tr("clickable") {
                        th {
                            +"Guid"
                        }
                        th {
                            +"Description"
                        }
                        th {
                            +"Count"
                        }
                    }
                }
                tbody {
                    bufflist.forEach {
                        tr {
                            td {
                                span(classes = "tdDrillDown") {
                                    attrs.onClickFunction = { evt ->
                                        window.open("https://classic.wowhead.com/spell=${it.guid}")
                                    }
                                    //11597/
                                    +it.guid
                                }
                            }
                            td {
                                +it.description
                            }
                            td {
                                +it.count.toString()
                            }
                        }
                    }
                }
            }
        }
        div("card-footer") {
            span {
                attrs.onClickFunction = {
                    footerClick()

                }
                +"close me"
            }
        }
    }
}