import io.ktor.client.*
import io.ktor.client.features.websocket.*
import kotlinx.coroutines.launch
import net.ideablender.whatAChad.pages.*
import net.ideablender.whatAChad.store.Store

import react.RBuilder
import react.RComponent
import react.RProps
import react.RState
import react.dom.div
import react.dom.li
import react.dom.ul
import react.router.dom.*


val client = HttpClient {
    install(WebSockets)
}



class Index : RComponent<RProps, RState>(){
    override fun componentDidMount() {
       Store.registerCallback("Index", ::onStoreChange)
    }

    private fun onStoreChange(){
        forceUpdate()
    }

    override fun RBuilder.render() {
        div("container-fluid"){
            browserRouter {
                div("navbar navbar-expand-md navbar-dark bg-dark fixed-top") {
                    routeLink(to ="/", className = "navbar-brand" ) { +"Home" }
                    div("collapse navbar-collapse"){
                        ul("navbar-nav mr-auto"){
                            li("nav-item"){
                                routeLink(to = "/about", className = "nav-link") { +"About" }
                            }
                            li("nav-item"){
                                routeLink(to = "/users", className = "nav-link") { +"Users" }
                            }
                            li("nav-item"){
                                routeLink(to = "/raid", className = "nav-link") { +"Raids" }
                            }
                            li("nav-item"){
                                routeLink(to = "/admin", className = "nav-link") { +"Admin" }
                            }
                            if(Store.isUserLoggedIn() == false){
                                li("nav-item"){
                                    routeLink(to = "/login", className = "nav-link") { +"Login" }
                                }
                            }
                        }
                    }
                }
                switch {
                    route("/about") { child(PAbout::class) {} }
                    route("/users") { child(PUsers::class) {} }
                    route("/login") { child(PLogin::class) {} }
                    route("/raid") { child(PRaid::class) {} }
                    route("/admin") {
                        if(Store.isUserLoggedIn()){
                            child(PAdmin::class) {}
                        }else{
                            child(PLogin::class) {}
                        }
                    }
                    route("/") { child(PHome::class) {} }
                }
            }
        }
    }
}