package net.ideablender.whatAChad

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast
import net.ideablender.whatAChad.models.*
import net.ideablender.whatAChad.req.UserREQ
import net.ideablender.whatAChad.usecase.user.UserCreate
import net.ideablender.whatAChad.usecase.user.UserCreateImpl
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.*

object BootStrap {
    private val userCreate: UserCreate = UserCreateImpl

    suspend fun loadData(){
        loadUsers()
/*        createRaid()
        createRaiders()
        createReportCards()
        createBuffs()
        createCasts()*/
    }

    private suspend fun loadUsers(){
        userCreate.create(UserCreate.Request(
            UserREQ(
                email = "Chris@ideablender.net",
                un = "root",
                password = "test123"
            )
        ))
    }

    private suspend fun createRaiders(){
        withContext(Dispatchers.IO) {
            transaction {
                val model = transaction{
                    RaidMember.new {
                        name = "Tomhighway"
                    }
                }
            }
        }
    }

    private suspend fun createReportCards(){
        withContext(Dispatchers.IO) {
            transaction {
                val _warror = RaidMember.all().first()
                val _raid = Raid.all().first()
                val model = transaction{
                    WarriorReportCard.new {
                        raidMember = _warror
                        raid = _raid
                    }
                }
            }
        }
    }

    private suspend fun createBuffs(){
        withContext(Dispatchers.IO) {
            transaction {
                val _rc = WarriorReportCard.all().first()
                val model = transaction{
                    BuffTotal.new {
                        guid = "1111"
                        count = 23
                        description = NotableBuff.ELIXIR_MONGOOSE.description
                        reportCard = _rc
                    }
                    BuffTotal.new {
                        guid = "222"
                        count = 4
                        description = NotableBuff.ELIXIR_GIANTS.description
                        reportCard = _rc
                    }
                }
            }
        }
    }
    private suspend fun createRaid() {
        withContext(Dispatchers.IO) {
            transaction {
                val model = transaction {
                    Raid.new {
                        uuid = "3234"
                        title = "Test Raid"
                        date = Date().time
                    }
                }
            }
        }
    }

    private suspend fun createCasts() {
        withContext(Dispatchers.IO) {
            transaction {
                val _rc = WarriorReportCard.all().first()
                val model = transaction{
                    CastTotal.new {
                        guid = "333"
                        count = 120
                        description = NotableCast.BLOODTHIRST_4.description
                        reportCard = _rc
                    }
                    CastTotal.new {
                        guid = "444"
                        count = 342
                        description =  NotableCast.SUNDER_ARMOR.description
                        reportCard = _rc
                    }
                }
            }
        }
    }
}

