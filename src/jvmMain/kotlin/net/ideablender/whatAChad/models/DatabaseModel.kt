package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin

interface DatabaseModel {
    fun toDTOMin():DTOMin
    fun toDTO():DTO
}