package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin
import net.ideablender.whatAChad.dtos.RaidDto
import net.ideablender.whatAChad.models.Users.uniqueIndex
import net.ideablender.whatAChad.models.WarriorReportCard.Companion.referrersOn
import net.ideablender.whatAChad.utils.toDateTime
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Raids : IntIdTable() {
    val uuid = varchar("uuid", 100).uniqueIndex()
    val title = varchar("title", 100)
    val date = long("date")
    val wlZoneId = integer("wl_zone_id")
}

class Raid(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<Raid>(Raids)
    var uuid by Raids.uuid
    var title by Raids.title
    var date by Raids.date
    var wlZoneId by Raids.wlZoneId
    val reports  by WarriorReportCard referrersOn WarriorReportCards.raid // @OneToMany

    override fun toDTOMin(): DTOMin = DTOMin(id=id.value, detail = title)

    override fun toDTO(): RaidDto {
       return RaidDto(
           id = id.value,
           wlZoneId = wlZoneId,
           uuid = uuid,
           title = title,
           date = date.toDateTime(),
           reports = reports.map { it.toDTOMin() }
       )
    }
}