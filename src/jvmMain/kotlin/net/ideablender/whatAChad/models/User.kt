package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.DTOMin
import net.ideablender.whatAChad.dtos.UserDTO
import net.ideablender.whatAChad.utils.toDateTime
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Users : IntIdTable() {
    val email = varchar("email", 100).uniqueIndex()
    val password = varchar("password", 100)
    val un = varchar("un", 100).uniqueIndex()
    val active  = bool("active")
    val isRoot  = bool("is_root")
}

class User(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<User>(Users)
    var email by Users.email
    var password by Users.password
    var un by Users.un
    var active by Users.active
    var isRoot by Users.isRoot

    override fun toDTOMin(): DTOMin = DTOMin(id = this.id.value, detail = un)

    override fun toDTO(): UserDTO {
        return UserDTO(
            id = this.id.value,
            email = email,
            un = un
        )
    }
}