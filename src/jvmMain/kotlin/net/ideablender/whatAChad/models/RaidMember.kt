package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin
import net.ideablender.whatAChad.dtos.RaidMemberDTO
import net.ideablender.whatAChad.models.Users.uniqueIndex
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object RaidMembers : IntIdTable() {
    val name = varchar("name", 100).uniqueIndex()
}
class RaidMember(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<RaidMember>(RaidMembers)
    var name by RaidMembers.name
    val reportCards  by WarriorReportCard referrersOn WarriorReportCards.raidMember // @OneToMany

    override fun toDTOMin(): DTOMin = DTOMin(id.value, name)

    override fun toDTO(): RaidMemberDTO {
        return RaidMemberDTO(
            id = id.value,
            name = name,
            reportCards = reportCards.map { it.toDTOMin() }
        )
    }
}