package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object CastTotals : IntIdTable() {
    val guid = varchar("guid", 100)
    val description = varchar("description", 100)
    val count = integer("count")
    val reportCard = reference("report_card", WarriorReportCards.id) // @ManyToOne
}
class CastTotal(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<CastTotal>(CastTotals)
    var guid by CastTotals.guid
    var count by CastTotals.count
    var description by CastTotals.description
    var reportCard by WarriorReportCard referencedOn CastTotals.reportCard  // @ManyToOne

    override fun toDTOMin(): DTOMin = DTOMin(id.value, description)

    override fun toDTO(): CastTotalDTO {
        return CastTotalDTO(
            id = id.value,
            guid = guid,
            count = count,
            description = description,
            reportCard = reportCard.toDTOMin()
        )
    }
}