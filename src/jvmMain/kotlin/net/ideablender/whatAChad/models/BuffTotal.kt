package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.BuffTotalDTO
import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object BuffTotals : IntIdTable() {
    val guid = varchar("guid", 100)
    val description = varchar("description", 100)
    val count = integer("count")
    val reportCard = reference("report_card", WarriorReportCards.id) // @ManyToOne
}
class BuffTotal(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<BuffTotal>(BuffTotals)
    var guid by BuffTotals.guid
    var count by BuffTotals.count
    var description by BuffTotals.description
    var reportCard by WarriorReportCard referencedOn BuffTotals.reportCard  // @ManyToOne

    override fun toDTOMin(): DTOMin  = DTOMin(id.value, description)

    override fun toDTO(): BuffTotalDTO {
        return BuffTotalDTO(
            id = id.value,
            guid = guid,
            count = count,
            description = description,
            reportCard = reportCard.toDTOMin()
        )
    }
}