package net.ideablender.whatAChad.models

import net.ideablender.whatAChad.dtos.DTO
import net.ideablender.whatAChad.dtos.DTOMin
import net.ideablender.whatAChad.dtos.WarriorReportCardDTO
import net.ideablender.whatAChad.models.RaidMember.Companion.referrersOn
import net.ideablender.whatAChad.models.RaidMembers.uniqueIndex
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object WarriorReportCards : IntIdTable() {
    val raidMember = reference("raid_member", RaidMembers.id) // @ManyToOne
    val raid = reference("raid", Raids.id) // @ManyToOne
    val spec = varchar("spec", 100)
}

class WarriorReportCard(id: EntityID<Int>) : IntEntity(id), DatabaseModel {
    companion object : IntEntityClass<WarriorReportCard>(WarriorReportCards)
    var spec by WarriorReportCards.spec
    var raidMember by RaidMember referencedOn WarriorReportCards.raidMember  // @ManyToOne
    var raid by Raid referencedOn WarriorReportCards.raid  // @ManyToOne
    val buffs  by BuffTotal referrersOn BuffTotals.reportCard // @OneToMany
    val casts  by CastTotal referrersOn CastTotals.reportCard // @OneToMany

    override fun toDTOMin(): DTOMin = DTOMin(id.value, raidMember.name)

    override fun toDTO(): WarriorReportCardDTO {
        return WarriorReportCardDTO(
            id = id.value,
            spec = spec,
            raidMember = raidMember.toDTOMin(),
            raid = raid.toDTOMin(),
            buffs = buffs.map { it.toDTOMin() },
            casts = casts.map { it.toDTOMin() }
        )
    }
}