package net.ideablender.whatAChad.usecase.user

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ideablender.whatAChad.exceptions.MissingMandatoryFieldException
import net.ideablender.whatAChad.helpers.LogHelper
import net.ideablender.whatAChad.models.User
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

object UserCreateImpl : UserCreate {
    val logger: Logger = LoggerFactory.getLogger(UserCreateImpl::class.simpleName)
    val modelClazz = "User"
    override suspend fun create(request: UserCreate.Request): UserCreate.Response {
        val req = request.req
        return try {
            val passwordHashed = BCrypt.hashpw(req.password, BCrypt.gensalt())
            withContext(Dispatchers.IO) {
                transaction {
                    val model = transaction{
                        User.new {
                            email = req.email?.toLowerCase() ?: throw MissingMandatoryFieldException(fieldName = "email")
                            password = passwordHashed
                            un = req.un ?: throw MissingMandatoryFieldException(fieldName = "un")
                            isRoot = false
                            active = false
                        }
                    }
                    UserCreate.Response.Success(model.toDTO())
                }
            }
        }catch (e:Exception){
            logger.error(LogHelper.uCCreateError(modelClazz, req, e))
            UserCreate.Response.Failure(e.message ?: "No exception Message")
        }
    }
}