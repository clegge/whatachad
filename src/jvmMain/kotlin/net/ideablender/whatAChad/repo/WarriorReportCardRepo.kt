package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory
import net.ideablender.whatAChad.db.ilike
import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.dtos.RaidDto
import net.ideablender.whatAChad.dtos.WarriorReportCardDTO
import net.ideablender.whatAChad.models.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.select

object WarriorReportCardRepo {
    suspend fun findAllDto(): List<WarriorReportCardDTO>? = DatabaseFactory.dbQuery {
        WarriorReportCard.all().map { it.toDTO() }
    }

    suspend fun findAllDtoByRaidId(raidId:Int): List<WarriorReportCardDTO>? = DatabaseFactory.dbQuery {
        WarriorReportCard.wrapRows(
            Raids.innerJoin(WarriorReportCards).select {  Raids.id eq raidId}
        ).map { it.toDTO() }
    }
}