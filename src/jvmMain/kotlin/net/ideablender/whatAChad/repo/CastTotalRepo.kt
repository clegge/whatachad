package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory
import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.dtos.WarriorReportCardDTO
import net.ideablender.whatAChad.models.CastTotal
import net.ideablender.whatAChad.models.CastTotals
import net.ideablender.whatAChad.models.WarriorReportCard
import net.ideablender.whatAChad.models.WarriorReportCards
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.select

object CastTotalRepo {
    suspend fun findAllDto(): List<CastTotalDTO>? = DatabaseFactory.dbQuery {
        CastTotal.all().map { it.toDTO() }
    }
    suspend fun findAllDtoByRaidId(raidId:Int): List<CastTotalDTO>? = DatabaseFactory.dbQuery {
        CastTotal.wrapRows(
            WarriorReportCards.innerJoin(CastTotals).select {  WarriorReportCards.id eq raidId}
        ).map { it.toDTO() }
    }
}