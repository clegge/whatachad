package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory
import net.ideablender.whatAChad.dtos.BuffTotalDTO
import net.ideablender.whatAChad.dtos.CastTotalDTO
import net.ideablender.whatAChad.models.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.select

object BuffTotalRepo {
    suspend fun findAllDto(): List<BuffTotalDTO>? = DatabaseFactory.dbQuery {
        BuffTotal.all().map { it.toDTO() }
    }

    suspend fun findAllDtoByRaidId(raidId:Int): List<BuffTotalDTO>? = DatabaseFactory.dbQuery {
        BuffTotal.wrapRows(
            WarriorReportCards.innerJoin(BuffTotals).select {  WarriorReportCards.id eq raidId}
        ).map { it.toDTO() }
    }
}