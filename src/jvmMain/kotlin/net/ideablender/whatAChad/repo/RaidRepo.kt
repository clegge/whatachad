package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory.dbQuery
import net.ideablender.whatAChad.db.ilike
import net.ideablender.whatAChad.dtos.RaidDto
import net.ideablender.whatAChad.dtos.RaidMemberDTO
import net.ideablender.whatAChad.models.*
import org.jetbrains.exposed.sql.select

object RaidRepo {
    suspend fun findByUuid(uuid:String): Raid? = dbQuery {
        Raid.find { Raids.uuid ilike uuid }.firstOrNull()
    }
    suspend fun findById(id:Int): RaidDto? = dbQuery {
        Raid.find { Raids.id eq id }.first().toDTO()
    }
    suspend fun findAllDto(): List<RaidDto>? = dbQuery {
        Raid.all().map{it.toDTO()}
    }
}