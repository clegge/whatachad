package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory.dbQuery
import net.ideablender.whatAChad.db.ilike
import net.ideablender.whatAChad.models.User
import net.ideablender.whatAChad.models.Users
import org.jetbrains.exposed.sql.select

object UserRepo {
    suspend fun findByUserName(userName:String): User? = dbQuery {
        User.find { Users.un ilike userName }.firstOrNull()
    }

    suspend fun findByEmail(email:String): User? = dbQuery {
        User.find { Users.email eq email }.firstOrNull()
    }
}