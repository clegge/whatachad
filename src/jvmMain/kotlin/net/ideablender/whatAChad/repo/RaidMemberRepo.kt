package net.ideablender.whatAChad.repo

import net.ideablender.whatAChad.db.DatabaseFactory.dbQuery
import net.ideablender.whatAChad.db.ilike
import net.ideablender.whatAChad.dtos.RaidDto
import net.ideablender.whatAChad.dtos.RaidMemberDTO
import net.ideablender.whatAChad.models.RaidMember
import net.ideablender.whatAChad.models.RaidMembers
import net.ideablender.whatAChad.models.User
import net.ideablender.whatAChad.models.Users
import org.jetbrains.exposed.sql.select

object RaidMemberRepo {
    suspend fun findByUserName(name:String): RaidMember? = dbQuery {
        RaidMember.find { RaidMembers.name ilike name }.firstOrNull()
    }

    suspend fun findAllDto(): List<RaidMemberDTO>? = dbQuery {
        RaidMember.all().map{it.toDTO()}
    }
}