package net.ideablender.whatAChad.db

import com.typesafe.config.ConfigFactory
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.config.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.ideablender.whatAChad.BootStrap
import net.ideablender.whatAChad.helpers.WarcraftLogHelper
import net.ideablender.whatAChad.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction


class ILikeOp(expr1: Expression<*>, expr2: Expression<*>) : ComparisonOp(expr1, expr2, "ILIKE")

infix fun<T:String?> ExpressionWithColumnType<T>.ilike(pattern: String): Op<Boolean> = ILikeOp(this, QueryParameter(pattern, columnType))

object DatabaseFactory {
    private val appConfig = HoconApplicationConfig(ConfigFactory.load())
    private val db_port = appConfig.property("db.port").getString()
    private val jwt_secret = appConfig.property("jwt.secret").getString()
    private val db_name = appConfig.property("db.name").getString()
    private val db_user = appConfig.property("db.name").getString()
    private val db_password = appConfig.property("db.dbPassword").getString()
    private val dbUrl = appConfig.property("db.jdbcUrl").getString().replace("PORT", db_port).replace("NAME", db_name)

    fun init() {
        Database.connect(hikari())
        transaction {
            SchemaUtils.drop(Users)
            SchemaUtils.drop(BuffTotals)
            SchemaUtils.drop(CastTotals)
            SchemaUtils.drop(WarriorReportCards)
            SchemaUtils.drop(RaidMembers)
            SchemaUtils.drop(Raids)
        }
        transaction {
            SchemaUtils.create(Users)
            SchemaUtils.create(WarriorReportCards)
            SchemaUtils.create(RaidMembers)
            SchemaUtils.create(BuffTotals)
            SchemaUtils.create(CastTotals)
            SchemaUtils.create(Raids)
        }
        GlobalScope.launch(Dispatchers.IO) {
            BootStrap.loadData()
        }
    }

    private fun hikari(): HikariDataSource {
        val config = HikariConfig()
        config.driverClassName = "org.postgresql.Driver"
        config.jdbcUrl = dbUrl
        config.username = db_user
        config.password = db_password
        config.maximumPoolSize = 3
        config.isAutoCommit = false
        config.transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        config.validate()
        return HikariDataSource(config)
    }

    suspend fun <T> dbQuery(block: () -> T): T =
            withContext(Dispatchers.IO) {
                transaction { block() }
            }
}