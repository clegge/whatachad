package net.ideablender.whatAChad.helpers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.ideablender.whatAChad.PATH_WARCRAFT_LOGS
import net.ideablender.whatAChad.STUB_API_KEY
import net.ideablender.whatAChad.WARCRAFT_LOGS_API_KEY
import net.ideablender.whatAChad.WARCRAFT_LOGS_USER
import net.ideablender.whatAChad.db.ilike
import net.ideablender.whatAChad.enums.HeroClass
import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast
import net.ideablender.whatAChad.enums.WarriorSpec
import net.ideablender.whatAChad.exceptions.FetchRaidExecutionException
import net.ideablender.whatAChad.exceptions.FetchReportException
import net.ideablender.whatAChad.models.*
import net.ideablender.whatAChad.pojos.*
import net.ideablender.whatAChad.repo.*
import net.ideablender.whatAChad.utils.toDateTime
import org.jetbrains.exposed.sql.transactions.transaction
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

object WarcraftLogHelper {
    private val logger: Logger = LoggerFactory.getLogger(WarcraftLogHelper::class.simpleName)

    private val _zones:MutableList<WLZone> = mutableListOf()

    private val _reports:MutableList<WLReport> = mutableListOf()

    fun getZones():List<WLZone> = _zones

    fun getReports() = _reports

    /*
    * Must be run when server starts.
    * url : https://classic.warcraftlogs.com/v1/zones?api_key=aa21b6a1e4187a5af3f17b04d752cb74
    * */
    fun fetchZones(){
        try {
            val gson = Gson()
            val response = khttp.get(
                "$PATH_WARCRAFT_LOGS/zones",
                params = mapOf(STUB_API_KEY to WARCRAFT_LOGS_API_KEY)
            )
            val zoneArray = response.jsonArray

            val ecounterTT = object : TypeToken<Array<WLEncounter>>() {}.type
            _zones.clear()
            zoneArray.forEach {
                it as JSONObject
                val encounters = it.get("encounters")
                val reportMembers: Array<WLEncounter> = gson.fromJson(encounters.toString(), ecounterTT)
                val zone = WLZone(
                    id= it.get("id") as Int,
                    name = it.get("name") as String,
                    encounters = gson.fromJson(encounters.toString(), ecounterTT)
                )
                _zones.add(zone)
            }
        }catch (e:Exception){
            logger.error("CATASTROPHIC ERROR, could not load zones from warcraft logs.")
        }
    }

    fun fetchReports() : Array<WLReport>{
        try{
            val gson = Gson()
            val arrayType = object : TypeToken<Array<WLReport>>() {}.type
            val response = khttp.get(
                "$PATH_WARCRAFT_LOGS/reports/user/$WARCRAFT_LOGS_USER",
                params = mapOf(STUB_API_KEY to WARCRAFT_LOGS_API_KEY)
            )
            val retObj: Array<WLReport> = gson.fromJson(response.text, arrayType)
            _reports.clear()
            _reports.addAll(retObj.toList())
            return retObj
        }catch (e:Exception){
            throw FetchReportException(e)
        }
    }

    fun buildCTXRaid(wlReportKey: String):CTXRaid{
        val raidExecution = WarcraftLogHelper.fetchRaidExecution(wlReportKey)
        val reportList: MutableList<WLWarriorReport> = mutableListOf()
        raidExecution.raiders.filter { it.type == HeroClass.WARRIOR.description }.forEach {
            val warrior = it
            var report = WLWarriorReport(
                raidId = raidExecution.wowRaidId,
                warrior = warrior
            )
            report = WarcraftLogHelper.populateBuffs(raidExecution, report)
            report = WarcraftLogHelper.populateCasts(raidExecution, report)
            reportList.add(report)
        }
        return CTXRaid(raidExecution, reportList)
    }

    private fun populateCasts(raid:WLRaidExecution, report: WLWarriorReport):WLWarriorReport{
            //https://classic.warcraftlogs.com/v1/report/tables/casts/mdc7rYMzTw8aLPC4?api_key=aa21b6a1e4187a5af3f17b04d752cb74&start=863&end=6318830&sourceid=29
        val response = khttp.get(
            "$PATH_WARCRAFT_LOGS/report/tables/casts/${raid.wowRaidId}",
            params = mapOf(
                STUB_API_KEY to WARCRAFT_LOGS_API_KEY,
                "start" to raid.fights.sortedBy { it.id }.first().start_time.toString(),
                "end" to raid.fights.sortedBy { it.id }.last().end_time.toString(),
                "sourceid" to report.warrior.id.toString()
            )
        )
        val root = response.jsonObject
        val entries = root.get("entries")
        entries as JSONArray
        entries.forEach {
            it as JSONObject
            try{
                val guid = it.get("guid") as Int
                val cast = NotableCast.values().find { it.guid == guid }
                if(cast != null){
                    var uptime:Int? = null
                    if(!it.isNull("uptime")){
                        uptime =it.get("uptime") as Int
                    }
                    val total = it.get("hitCount") as Int
                    val bt = WLRaidCastTotal(
                        cast = cast,
                        id = UUID.randomUUID().toString(),
                        count = total,
                        raider = report.warrior,
                        uptime = uptime
                    )
                    report.casts.add(bt)
                    val foo = true
                }



            }catch (e:Exception){
                val foo = true
            }
        }
        return report
    }

    private fun populateBuffs(raid:WLRaidExecution, report: WLWarriorReport):WLWarriorReport{
        val response = khttp.get(
            "$PATH_WARCRAFT_LOGS/report/tables/buffs/${raid.wowRaidId}",
            params = mapOf(
                STUB_API_KEY to WARCRAFT_LOGS_API_KEY,
                "start" to raid.fights.sortedBy { it.id }.first().start_time.toString(),
                "end" to raid.fights.sortedBy { it.id }.last().end_time.toString(),
                "targetid" to report.warrior.id.toString()
            )
        )
        val root = response.jsonObject
        val entries = root.get("auras")
        entries as JSONArray
        entries.forEach {
            it as JSONObject
            val name = it.get("name") as String
            val guid = it.get("guid") as Int
            val uptime = it.get("totalUptime") as Int
            val total = it.get("totalUses") as Int
            val buff = NotableBuff.values().find { it.guid == guid }
            if(buff != null){
                val bt = WLRaidBuffTotal(
                    buff = buff,
                    id = UUID.randomUUID().toString(),
                    count = total,
                    raider = report.warrior,
                    uptime = uptime
                )
                report.buffs.add(bt)
                val foo = true
            }else{
                logger.info("*** UKNOWN BUFF : $name : $guid")
            }
        }
        return report
    }

    /*
    * URL: // https://classic.warcraftlogs.com/v1/report/fights/mdc7rYMzTw8aLPC4?api_key=aa21b6a1e4187a5af3f17b04d752cb74
    * */
    fun fetchRaidExecution(wowLogRaidId:String):WLRaidExecution{
        try {
            val gson = Gson()

            val response = khttp.get(
                "$PATH_WARCRAFT_LOGS/report/fights/$wowLogRaidId",
                params = mapOf(STUB_API_KEY to WARCRAFT_LOGS_API_KEY)
            )

            val start = Date(response.jsonObject.get("start") as Long)
            val end = Date(response.jsonObject.get("end") as Long)

            val wLFightTT = object : TypeToken<Array<WLFight>>() {}.type
            val wLFightObj = response.jsonObject.get("fights")
            val reportFights: Array<WLFight> = gson.fromJson(wLFightObj.toString(), wLFightTT)

            val wlRaidMemberTT = object : TypeToken<Array<WLRaidMember>>() {}.type
            val wlRaidMemberObj = response.jsonObject.get("friendlies")
            val reportMembers: Array<WLRaidMember> = gson.fromJson(wlRaidMemberObj.toString(), wlRaidMemberTT)

            val raidExecution = WLRaidExecution(
                id = UUID.randomUUID().toString(),
                wowRaidId = wowLogRaidId,
                raiders = reportMembers.filter { rm -> HeroClass.values().find { it.description == rm.type } != null },
                timeStart = start.toDateTime(),
                timeEnd = end.toDateTime(),
                fights = reportFights.toList()
            )
            return raidExecution
        }catch (e:Exception){
            val tossit = FetchRaidExecutionException(id=wowLogRaidId, e = e)
            logger.error(tossit.message)
            throw tossit
        }
    }

    private fun determineSpec(rep:WLWarriorReport) : WarriorSpec{
        val casts = rep.casts
        var spec = WarriorSpec.UNKOWN
        val ss = casts.find { it.cast == NotableCast.SHIELD_SLAM }
        val ms = casts.find { it.cast == NotableCast.MS }
        val btCount =rep.getBloodThirst()?.count ?: 0
        val sunderCount = casts.find { it.cast == NotableCast.SUNDER_ARMOR }?.count ?: 0
        val wwCount =casts.find { it.cast == NotableCast.WHIRLWIND }?.count ?: 0
        val sbCount =casts.find { it.cast == NotableCast.SHIELD_BLOCK }?.count ?: 0
        val tauntCount = casts.find { it.cast == NotableCast.TAUNT }?.count ?: 0
        val revCount =rep.getRevenge()?.count ?: 0
        if(ss != null){
            return WarriorSpec.PROT
        }
        if(ms != null){
            return WarriorSpec.ARMS
        }
        if(btCount != 0){
            if(btCount > (sunderCount + sbCount + tauntCount + revCount)){
                return WarriorSpec.FURY
            }else{
                return WarriorSpec.FURY_PROT
            }
        }
        return spec
    }


    fun createRaid(report: WLReport){
        try {
            val raidCtx = buildCTXRaid(report.id)
            transaction {
                var _raid = Raid.find { Raids.uuid ilike raidCtx.raidExecution.wowRaidId }.firstOrNull()
                if(_raid == null){
                    _raid = Raid.new {
                        uuid = raidCtx.raidExecution.wowRaidId
                        title = report.title
                        date = report.start
                        wlZoneId = report.zone
                    }

                    raidCtx.reports.forEach {wlReport ->
                        var _warrior= RaidMember.find { RaidMembers.name ilike wlReport.warrior.name  }.firstOrNull()
                        val _spec = determineSpec(wlReport)
                        if(_warrior == null){
                            logger.info("Adding new warrior: ${wlReport.warrior.name}")
                            _warrior = RaidMember.new {
                                name =wlReport.warrior.name
                            }
                        }
                        val _reportCard = WarriorReportCard.new {
                            raidMember = _warrior
                            raid = _raid
                            spec = _spec.name
                        }
                        wlReport.casts.forEach { wlCast ->
                            val _cast = CastTotal.new {
                                guid = wlCast.cast.guid.toString()
                                count = wlCast.count
                                description = wlCast.cast.description
                                reportCard = _reportCard
                            }
                        }
                        wlReport.buffs.forEach { wlBuff ->
                            val _buff = BuffTotal.new {
                                guid = wlBuff.buff.guid.toString()
                                count = wlBuff.count
                                description = wlBuff.buff.description
                                reportCard = _reportCard
                            }
                        }
                    }
                }else{
                    logger.info("Raid for warcraft log id ${raidCtx.raidExecution.wowRaidId} already exists")
                }
            }
        }catch (e:Exception){
            logger.error("createRaid for ${report.id} failed : ${e.message}")
        }
    }
}