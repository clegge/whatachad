package net.ideablender.whatAChad.helpers

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import server

object SocketBroadcaster {
    private val logger: Logger = LoggerFactory.getLogger(SocketBroadcaster::class.simpleName)
    var messageConsume = true

    var sockMessage = "First Message"

    suspend fun broadCast(msg:String){
        server.broadcastToAll(msg)
        logger.info("SocketBroadcaster sent $msg")
    }
}