package net.ideablender.whatAChad.utils

import java.util.*

actual typealias Date = java.util.Date

actual fun DateTime.compareToNative(date: Date): Int {
    val inDateAsDateTime =date.toDateTime()
    return this.compareTo(inDateAsDateTime)
}

actual fun DateTime.toNative(): Date {
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.YEAR, this.year)
    calendar.set(Calendar.MONTH, this.month)
    calendar.set(Calendar.DAY_OF_MONTH, this.day)
    calendar.set(Calendar.HOUR_OF_DAY, this.hour)
    calendar.set(Calendar.MINUTE, this.minute)
    calendar.set(Calendar.SECOND, this.second)
    calendar.set(Calendar.MILLISECOND, this.milliSecond)
    return calendar.time
}

actual fun DateTime.compareTo(date: DateTime): Int {
    val thisAsNative = this.toNative()
    val compareDate = date.toNative()
    return thisAsNative.compareTo(compareDate)
}

actual fun DateTime.eqNative(date: Date): Boolean =  this.compareToNative(date) == 0

actual fun DateTime.isBefore(date: DateTime): Boolean = this.compareTo(date) < 0

actual fun DateTime.isBeforeNative(date: Date): Boolean = this.compareToNative(date) < 0

actual fun DateTime.isAfter(date: DateTime): Boolean = this.compareTo(date) > 0

actual fun DateTime.isAfterNative(date: Date): Boolean = this.compareToNative(date) > 0

fun java.util.Date.toDateTime(): DateTime {
    val calendar = GregorianCalendar()
    calendar.time = this
    return DateTime(year = calendar.get(Calendar.YEAR), month = calendar.get(Calendar.MONTH), day = calendar.get(Calendar.DAY_OF_MONTH), hour = calendar.get(Calendar.HOUR_OF_DAY), minute = calendar.get(Calendar.MINUTE), second = calendar.get(Calendar.SECOND))
}

fun Long.toDateTime():DateTime{
    val c = Calendar.getInstance()
    c.timeInMillis = this
    return c.time.toDateTime()
}