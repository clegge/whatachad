import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.config.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.samples.chat.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.sessions.*
import io.ktor.util.*
import io.ktor.websocket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.ideablender.whatAChad.SimpleJWT
import net.ideablender.whatAChad.db.DatabaseFactory
import net.ideablender.whatAChad.dtos.*
import net.ideablender.whatAChad.helpers.WarcraftLogHelper
import net.ideablender.whatAChad.models.*
import net.ideablender.whatAChad.repo.*
import net.ideablender.whatAChad.req.LoginReq
import net.ideablender.whatAChad.utils.toDateTime
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration


data class ChatSession(val id: String)

val server = ChatServer()

fun main() {
    val port = System.getenv("PORT")?.toInt() ?: 9090
    val logger: Logger = LoggerFactory.getLogger("Server.kt")
    val appConfig = HoconApplicationConfig(ConfigFactory.load())

    logger.info("=========================== Application Started ===========================")
    DatabaseFactory.init()
    WarcraftLogHelper.fetchZones()
    WarcraftLogHelper.fetchReports()
    embeddedServer(Netty, port) {
        install(ContentNegotiation) {
            json()
        }
        install(CallLogging)
        install(DefaultHeaders)
        install(CORS) {
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            method(HttpMethod.Delete)
            anyHost()
        }
        install(Compression) {
            gzip()
        }
        install(WebSockets) {
            pingPeriod = Duration.ofMinutes(1)
        }
        install(Sessions) {
            cookie<ChatSession>("SESSION")
        }

        val simpleJwt = SimpleJWT(appConfig.property("jwt.secret").toString())

       // jwt_secret = appConfig.property("jwt.secret").getString()
        install(Authentication) {
            jwt {
                verifier(simpleJwt.verifier)
                validate {
                    UserIdPrincipal(it.payload.getClaim("name").asString())
                }
            }
        }

        intercept(ApplicationCallPipeline.Features) {
            if (call.sessions.get<ChatSession>() == null) {
                call.sessions.set(ChatSession(generateNonce()))
            }
        }

        routing {
            webSocket("/ws") { // websocketSession
                val session = call.sessions.get<ChatSession>()
                if (session == null) {
                    close(CloseReason(CloseReason.Codes.VIOLATED_POLICY, "No session"))
                    return@webSocket
                }
                server.memberJoin(session.id, this)
                for (frame in incoming) {
                    when (frame) {
                        is Frame.Text -> {
                            val text = frame.readText()
                            outgoing.send(Frame.Text("YOU SAID: $text Thats my car"))
                            if (text.equals("bye", ignoreCase = true)) {
                                close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                            }
                        }
                    }
                }
            }
        }
        routing {
            get("/") {
                call.application.environment.log.info("this is a test")
                call.respondText(
                    this::class.java.classLoader.getResource("index.html")!!.readText(),
                    ContentType.Text.Html
                )
            }
            static("/") {
                resources("")
            }

            route("/foobar") {
                get {
                    call.respond("Rest Is Working")
                }
            }

            get(ZoneDTO.getPath()){
                call.respond(WarcraftLogHelper.getZones().map { it.toDto() })
            }
            post(LoginReq.getPath()) {
                val post = call.receive<LoginReq>()
                val user = UserRepo.findByUserName(post.userName)
                if (user == null || !BCrypt.checkpw(post.password, user.password)) {
                    call.respond(LoginDTO(success = false))
                }
                call.respond(LoginDTO(success = true,token = simpleJwt.sign(user!!.email)))
            }

            authenticate {
                get(WLReportDTO.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val wlReps :MutableList<WLReportDTO> = mutableListOf()
                    WarcraftLogHelper.getReports().forEach {
                        wlReps.add(WLReportDTO(id = it.id, title = it.title, started = it.start.toDateTime(), ended = it.end.toDateTime()))
                    }
                    call.respond(wlReps)
                }
                get(RaidMemberDTO.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val wlReps :MutableList<WLReportDTO> = mutableListOf()
                    val resp = RaidMemberRepo.findAllDto()
                    call.respond(resp!!)
                }
                get(RaidDto.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val resp = RaidRepo.findAllDto()
                    call.respond(resp!!)
                }
                get(WarriorReportCardDTO.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val resp = WarriorReportCardRepo.findAllDto()
                    call.respond(resp!!)
                }

                get(BuffTotalDTO.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val resp = BuffTotalRepo.findAllDto()
                    call.respond(resp!!)
                }



                get(CastTotalDTO.getPath()){
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    val resp = CastTotalRepo.findAllDto()
                    call.respond(resp!!)
                }

                get("/protected" ) {
/*                    call.respond(userService.getAllUsers())
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = userService.getUserByEmail(userEmail)?: error("user not found")
                    if (!user.active) {
                        error("user not active")
                    }
                    call.respond(userService.getAllUsers())*/
                    val principal = call.principal<UserIdPrincipal>() ?: error("No principal decoded")
                    val userEmail = principal.name
                    val user = UserRepo.findByEmail(userEmail) ?: error("user not found")
                    call.respond("hello")
                }
            }
        }

        val _zones = WarcraftLogHelper.getZones()

        GlobalScope.launch(Dispatchers.IO) {
            WarcraftLogHelper.createRaid(WarcraftLogHelper.getReports().first())
            WarcraftLogHelper.createRaid(WarcraftLogHelper.getReports().last())
        }

/*


        GlobalScope.launch(Dispatchers.IO) {
            RaidRepo.findAllDto()?.forEach { _raid ->
                val raid = RaidRepo.findById(_raid.id)
                val casts = CastTotalRepo.findAllDtoByRaidId(_raid.id)
                val buffs = BuffTotalRepo.findAllDtoByRaidId(_raid.id)
                var cards = WarriorReportCardRepo.findAllDtoByRaidId(_raid.id)
                val bar = true
            }
        }*/




        val started = true
    }.start(wait = true)
}



