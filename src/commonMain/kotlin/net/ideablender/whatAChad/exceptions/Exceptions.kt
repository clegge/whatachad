package net.ideablender.whatAChad.exceptions

class MissingMandatoryFieldException(fieldName: String) :
    Exception("The field $fieldName is mandatory, but was not supplied")

data class ClientNotFoundException(val id: Int) : Exception("Could not find a Client with id : $id")
data class KioskNotFoundException(val id: Int) : Exception("Could not find a Kiosk with id : $id")
data class HeroClassNotFoundException(val id: String) : Exception("Could not find a HeroClass with id : $id")

class FetchReportException(e: Exception) : Exception("Application failed to fetch reports from warcraft logs : ${e.message}")
class FetchRaidExecutionException(e: Exception, id:String) : Exception("Application failed to fetch WLRaidExecution from warcraft logs for id: $id. Error: ${e.message}")
class WLRaidMemberNotFoundException(name:String) : Exception("Could not find a WLRaidMember for name: $name.")
class ClientTokenNotFoundException() : Exception("Client Token Not Present In Store")
