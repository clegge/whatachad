package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable

@Serializable
data class LoginDTO(
    val success:Boolean,
    val token :String? = null
): DTO {

}