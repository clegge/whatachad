package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable
import net.ideablender.whatAChad.utils.DateTime

@Serializable
data class RaidDto(
    val id:Int,
    val wlZoneId:Int,
    val uuid :String,
    val title :String,
    val date :DateTime,
    val reports:List<DTOMin>
): DTO {
    companion object {
        fun getPath() = "/raidReports"
    }
}