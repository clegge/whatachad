package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable
import net.ideablender.whatAChad.utils.DateTime

@Serializable
data class CastTotalDTO(
    val id:Int,
    val guid :String,
    val count :Int,
    val description :String,
    val reportCard:DTOMin
): DTO {
    companion object {
        fun getPath() = "/castTotals"
    }
}