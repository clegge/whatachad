package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable

@Serializable
data class UserDTO(
    val id:Int,
    val un :String,
    val email:String
): DTO {
    companion object {
        fun getPath() = "/Users"
    }
}