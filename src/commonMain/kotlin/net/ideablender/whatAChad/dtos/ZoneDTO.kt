package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable


@Serializable
data class ZoneDTO(
    val id:Int,
    val name :String
): DTO {
    companion object {
        fun getPath() = "/Zones"
    }
}