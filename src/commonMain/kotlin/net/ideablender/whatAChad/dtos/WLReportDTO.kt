package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable
import net.ideablender.whatAChad.utils.DateTime

@Serializable
data class WLReportDTO(
    val id:String,
    val title:String,
    val started:DateTime,
    val ended:DateTime
){
    companion object {
        fun getPath() = "/wLReports"
    }
}