package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable

@Serializable
data class DTOMin(
    val id: Int,
    val detail: String
)