package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable

@Serializable
data class RaidMemberDTO(
    val id:Int,
    val name:String,
    val reportCards:List<DTOMin>
): DTO {
    companion object {
        fun getPath() = "/raidMembers"
    }
}