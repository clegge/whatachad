package net.ideablender.whatAChad.dtos

import kotlinx.serialization.Serializable
import net.ideablender.whatAChad.utils.DateTime

@Serializable
data class WarriorReportCardDTO(
    val id:Int,
    val raidMember :DTOMin,
    val raid :DTOMin,
    val spec :String,
    val buffs:List<DTOMin>,
    val casts:List<DTOMin>
): DTO {
    companion object {
        fun getPath() = "/raidReportCards"
    }
}