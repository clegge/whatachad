package net.ideablender.whatAChad.utils

import kotlinx.serialization.Serializable

expect class Date

@Serializable
data class DateTime(
    val year: Int = 0,
    val month: Int = 0,
    val day: Int = 0,
    val hour: Int = 0,
    val minute: Int = 0,
    val second: Int = 0,
    val milliSecond: Int = 0,
    val jdTime: Int = 0
) {
    /*
    * should return SimpleDateFormat("MM/dd/yyyy hh.mm aa")
    * */
    fun formatShort(): String {
        val montAdjusted = month + 1
        var hourAdjusted = hour
        if (hour > 12) {
            hourAdjusted = hourAdjusted - 12
        }
        return "${montAdjusted.toTwoDigitString()}/${day.toTwoDigitString()}/$year ${hourAdjusted.toTwoDigitString()}:${minute.toTwoDigitString()} ${hour.toAmPm()}"
    }

    fun formatShortNoTime(): String = "$month/$day/$year"

    fun compareTo(other: DateTime): Int {
        if (this.year > other.year) return 1
        if (this.year < other.year) return -1
        if (this.month > other.month) return 1
        if (this.month < other.month) return -1
        if (this.day > other.day) return 1
        if (this.day < other.day) return -1
        if (this.second > other.second) return 1
        if (this.second < other.second) return -1
        if (this.milliSecond > other.milliSecond) return 1
        if (this.milliSecond < other.milliSecond) return -1
        return 0
    }
}

fun Int.toAmPm(): String {
    if (this > 24) throw Exception("Int must be less than 24, you supplied $this.")
    return if (this < 12) {
        "AM"
    } else {
        "PM"
    }
}

fun Int.toTwoDigitString(): String {
    if (this > 100) throw Exception("Int must be less than 100, you supplied $this.")
    return if (this < 10) {
        "0$this"
    } else {
        this.toString()
    }
}

expect fun DateTime.toNative(): Date
expect fun DateTime.compareTo(date: DateTime): Int
expect fun DateTime.compareToNative(date: Date): Int
expect fun DateTime.eqNative(date: Date): Boolean
expect fun DateTime.isBefore(date: DateTime): Boolean
expect fun DateTime.isBeforeNative(date: Date): Boolean
expect fun DateTime.isAfter(date: DateTime): Boolean
expect fun DateTime.isAfterNative(date: Date): Boolean