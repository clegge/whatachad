package net.ideablender.whatAChad.validation

data class FieldValidationResult(val fieldName: String, var valid: Boolean = true, var displayMessage: String? = null)