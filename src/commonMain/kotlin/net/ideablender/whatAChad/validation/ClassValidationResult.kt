package net.ideablender.whatAChad.validation

data class ClassValidationResult(
    val clazz: String,
    val invalidFieldResults: MutableList<FieldValidationResult> = mutableListOf()
) {
    fun isValid(): Boolean = invalidFieldResults.size == 0
    fun addResults(fieldValidationResult: FieldValidationResult) {
        if (!fieldValidationResult.valid) {
            invalidFieldResults.add(fieldValidationResult)
        }
    }
}