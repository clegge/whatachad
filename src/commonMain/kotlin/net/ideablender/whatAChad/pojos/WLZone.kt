package net.ideablender.whatAChad.pojos

import net.ideablender.whatAChad.dtos.ZoneDTO

data class WLZone(
    val id:Int,
    val name:String,
    val encounters:Array<WLEncounter>
){
    fun toDto() = ZoneDTO(id, name)
}