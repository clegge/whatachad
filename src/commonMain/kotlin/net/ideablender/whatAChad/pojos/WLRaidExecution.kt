package net.ideablender.whatAChad.pojos

import net.ideablender.whatAChad.utils.DateTime

data class WLRaidExecution(
    val id: String,
    val wowRaidId: String,
    val raiders:List<WLRaidMember>,
    val fights:List<WLFight>,
    val timeStart:DateTime,
    val timeEnd:DateTime
)