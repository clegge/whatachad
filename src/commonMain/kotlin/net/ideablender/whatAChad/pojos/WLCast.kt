package net.ideablender.whatAChad.pojos

data class WLCast(
    val name:String,
    val guid:Int,
    val hit:Int,
    val miss:Int,
    val uptime:Long
)