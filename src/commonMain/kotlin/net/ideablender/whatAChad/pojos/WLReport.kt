package net.ideablender.whatAChad.pojos

data class WLReport(
    val id: String,
    val title: String,
    val owner: String,
    val start: Long,
    val end: Long,
    val zone: Int
)