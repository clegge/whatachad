package net.ideablender.whatAChad.pojos

data class WLFight(
    val id:Int,
    val start_time:Long,
    val end_time:Long,
    val boss:Int,
    val originalBoss:Int,
    val name:String
)