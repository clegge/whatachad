package net.ideablender.whatAChad.pojos

data class WLRaidMember(
    val name:String,
    val id:Int,
    val guid:Int,
    val type:String,
    val server:String,
    val icon:String,
    val fights:Array<WLFightParticipation> = arrayOf()
)