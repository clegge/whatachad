package net.ideablender.whatAChad.pojos

data class CTXRaid (
    val raidExecution:WLRaidExecution,
    val reports:MutableList<WLWarriorReport> = mutableListOf()
)