package net.ideablender.whatAChad.pojos

import net.ideablender.whatAChad.enums.NotableBuff

data class WLRaidBuffTotal(
    val id:String,
    val count:Int,
    val buff:NotableBuff,
    val raider:WLRaidMember,
    val uptime:Int
)