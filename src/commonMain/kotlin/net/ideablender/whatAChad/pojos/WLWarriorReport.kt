package net.ideablender.whatAChad.pojos

import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast

data class WLWarriorReport(
    val raidId:String,
    val warrior:WLRaidMember,
    val buffs:MutableList<WLRaidBuffTotal> = mutableListOf(),
    val casts:MutableList<WLRaidCastTotal> = mutableListOf()
){
    fun getBuffTotalFor(_buff:NotableBuff):WLRaidBuffTotal? = buffs.find { it.buff == _buff }
    fun getCastTotalFor(_cast:NotableCast):WLRaidCastTotal? = casts.find { it.cast == _cast }


    /*
    * Helper methods for casts with multiple ranks (folks getting aq books, not training)
    * */
    fun getBloodThirst():WLRaidCastTotal?{
        var bt:WLRaidCastTotal? = null
        bt = casts.find { it.cast == NotableCast.BLOODTHIRST_1 || it.cast == NotableCast.BLOODTHIRST_4  }
        return bt
    }

    fun getRevenge():WLRaidCastTotal?{
        var rev:WLRaidCastTotal? = null
        rev = casts.find { it.cast == NotableCast.REVENGE_5 || it.cast == NotableCast.REVENGE_5  }
        return rev
    }



    fun getBattleShout():WLRaidCastTotal? = casts.find { it.cast == NotableCast.BATTLE_SHOUT_6 || it.cast == NotableCast.BATTLE_SHOUT_7  }


    fun getHeroicStrike():WLRaidCastTotal? = casts.find { it.cast == NotableCast.HEROIC_STRIKE_8 || it.cast == NotableCast.HEROIC_STRIKE_9  }
}