package net.ideablender.whatAChad.pojos

import net.ideablender.whatAChad.enums.NotableBuff
import net.ideablender.whatAChad.enums.NotableCast

data class WLRaidCastTotal(
    val id:String,
    val count:Int,
    val cast:NotableCast,
    val raider:WLRaidMember,
    val uptime:Int? = null
)