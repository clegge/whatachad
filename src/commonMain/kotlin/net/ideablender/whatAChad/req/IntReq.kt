package net.ideablender.whatAChad.req

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import net.ideablender.whatAChad.validation.ClassValidationResult

@Serializable
data class IntReq(
    var value: Int,
    override var id: Int? = 0
) : REQ {
    override fun toJson(): String {
        return Json.stringify(serializer(), this)
    }

    override fun validate(): ClassValidationResult {
        TODO("Not yet implemented")
    }
}