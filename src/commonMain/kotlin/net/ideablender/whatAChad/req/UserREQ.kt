package net.ideablender.whatAChad.req

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import net.ideablender.whatAChad.validation.ClassValidationResult

@Serializable
data class UserREQ(
    override var id: Int? = null,
    var email: String?,
    var password: String?,
    var un: String?
) : REQ {
    override fun toJson(): String {
        return Json.stringify(serializer(), this)
    }

    override fun validate(): ClassValidationResult {
        TODO("Not yet implemented")
    }
}