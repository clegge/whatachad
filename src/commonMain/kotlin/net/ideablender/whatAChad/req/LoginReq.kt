package net.ideablender.whatAChad.req

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import net.ideablender.whatAChad.validation.ClassValidationResult

@Serializable
data class LoginReq(
    var userName: String,
    var password: String,
    override var id: Int? = 0
) : REQ {
    override fun toJson(): String {
        return Json.stringify(serializer(), this)
    }

    override fun validate(): ClassValidationResult {
        TODO("Not yet implemented")
    }
    companion object {
        fun getPath() = "/Users"
    }
}