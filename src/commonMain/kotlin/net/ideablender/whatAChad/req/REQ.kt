package net.ideablender.whatAChad.req

import net.ideablender.whatAChad.validation.ClassValidationResult

interface REQ {
    var id: Int?
    fun validate(): ClassValidationResult
    fun toJson(): String
}

