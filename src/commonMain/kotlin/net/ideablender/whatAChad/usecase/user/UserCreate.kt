package net.ideablender.whatAChad.usecase.user

import net.ideablender.whatAChad.dtos.UserDTO
import net.ideablender.whatAChad.req.UserREQ

interface UserCreate {
    suspend fun create(request: Request): Response
    data class Request(val req: UserREQ)
    sealed class Response {
        class Failure(val errMsg: String) : Response()
        class Success(val dto: UserDTO) : Response()
    }
}