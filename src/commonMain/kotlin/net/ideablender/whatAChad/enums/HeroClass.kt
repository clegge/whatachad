package net.ideablender.whatAChad.enums

enum class HeroClass(val description:String) {
    DRUID("Druid"),
    MAGE("Mage"),
    ROGUE("Rogue"),
    SHAMAN("Shaman"),
    WARRIOR("Warrior"),
    WARLOCK("Warlock"),
    PRIEST("Priest"),
    HUNTER("Hunter"),
}