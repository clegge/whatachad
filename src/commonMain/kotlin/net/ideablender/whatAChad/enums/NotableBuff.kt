package net.ideablender.whatAChad.enums

enum class NotableBuff(val guid:Int, val description:String) {
    ARMOR(11349, "Elixir of Greater Defense"),
    ELIXIR_GIANTS(11405, "Elixir of the Giants"),
    ELIXIR_MONGOOSE(17538, "Elixir of the Mongoose"),
    FIRE_PROTECTION_GREATER(17543, "Greater Fire Protection Potion"),
    FIRE_PROTECTION(7233, "Fire Protection Potion"),
    FLASK_TITANS(17626, "Flask of the Titans"),
    FLURRY(12970, "Flurry"),
    FAP(6615, "Free Action"),
    ARTHAS(11371, "GIFT_OF_ARTHAS"),
    GORDOCK_GREEN(22789, "Gordok Green Grog"),
    GREATER_ARMOR(11348, "Greater Armor"),
    GREATER_STONESHIELD(17540, "Greater Stoneshield"),
    LIP(3169, "Invulnerability"),
    LAST_STAND(12976, "Last Stand"),
    MIGHTY_RAGE(17528, "Mighty Rage"),
    ROIDS(10667, "Rage of Ages"),
    RESTORATION(11359, "Restorative Potion"),
    RUMSEY_RUM(25804, "Rumsey Rum Black Label"),
    STRIKE_OF_SCORPOCK(10669, "Strike of the Scorpok"),
    SPIRIT_OF_ZANZA(24382, "Spirit of Zanza"),
    WF_TOTEM_3(10610, "WindFury Totem (R3)"),
    WF_TOTEM_1(8516, "WindFury Totem (R1)"),
    WINTERFALL_FIREWATER(17038, "Winterfall Firewater")
}