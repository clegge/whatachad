package net.ideablender.whatAChad.enums

enum class WarriorSpec(val description:String) {
    ARMS("Arms"),
    PROT("Prot"),
    FURY("Fury"),
    FURY_PROT("Fury-Prot"),
    UNKOWN("Unknown")
}